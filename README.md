# Coq-Quellen zum Beitrag: *Schritte zu einer zertifizierten Informationsflussanalyse von Geschäftsprozessen*

[![build status](https://gitlab.com/t.heinze/zeus2018/badges/master/pipeline.svg)](https://gitlab.com/t.heinze/zeus2018/commits/master)

Siehe auch [https://gitlab.com/t.heinze/soca2018](https://gitlab.com/t.heinze/soca2018)

Die Coq-Quellen beruhen auf Adam Chlipalas [Coq-Formalisierung zur Andersen-Analyse](http://adam.chlipala.net/itp/coq/src/)

## Referenzen

 * [Thomas S. Heinze: Schritte zu einer zertifizierten Informationsflussanalyse von Geschäftsprozessen. ZEUS 2018](http://ceur-ws.org/Vol-2072/paper5.pdf)
 * [Thomas S. Heinze, Jasmin Türker: Certified Information Flow Analysis of Service Implementations. SOCA 2018](https://doi.org/10.1109/SOCA.2018.00033)
