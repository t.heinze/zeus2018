let request = O
let score = S request 
let rating = S score

let risk = O

let myProc = Sequence (
                      (Activity (Allocate request)),
                      (Sequence (
                        (Activity (Source score)),
                        (Sequence (
                          (Activity (Write (request, risk, score))),
                          (Sequence (
                            (Choice (
                              (Activity (Read (rating, request, risk))),
                              (Activity (Allocate rating)))),
                            (Empty))))))))

let result = (match analysis myProc rating with
  | NotTainted -> "Not tainted"
  | MaybeTainted -> "Maybe tainted")

let () = Printf.printf "Variable rating: %s\n" (result)
