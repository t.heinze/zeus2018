(** Points-to analysis *)

Require Import Arith List.
Require Import Machine Maps.

Set Implicit Arguments.

(** * The instruction language *)

Definition field := nat.
Definition var := nat.

Inductive activity : Set :=
| Allocate : var -> activity
| Source : var -> activity
| Copy : var -> var -> activity
| Sanitize : var -> var -> activity
| Read : var -> var -> field -> activity
| Write : var -> field -> var -> activity.

(** * Machine states *)

Module NatEq <: EQ.
  Definition dom := nat.
  Definition dom_eq_dec := eq_nat_dec.
End NatEq.
Module NatMap := FuncMap(NatEq).

Module VarMap : MAP with Definition dom := var := NatMap.

Lemma eq_natboolpair_dec : forall x y : nat*bool, {x=y}+{~x=y}.
Proof. repeat decide equality. Qed.

Module NatBoolPairEq <: EQ.
  Definition dom : Set := (nat*bool)%type.
  Definition dom_eq_dec := eq_natboolpair_dec.
End NatBoolPairEq.

Lemma eq_natpair_dec : forall x y : nat*nat, {x=y}+{~x=y}.
Proof. repeat decide equality. Qed.

Module NatPairEq <: EQ.
  Definition dom : Set := (nat*nat)%type.
  Definition dom_eq_dec := eq_natpair_dec.
End NatPairEq.

Module NatPairMap := FuncMap(NatPairEq).

Definition object := (nat*bool)%type.
Definition cell (o : object) := match o with (x, y) => x end.
Definition taint (o : object) := match o with (x, y) => y end.

Definition allocation_site := nat.

Module AllocMap : MAP with Definition dom := (allocation_site*field)%type:= NatPairMap.
Module AllocSet : SET with Definition dom := object := ListSet(NatBoolPairEq).

Record state : Set := {
  vars : VarMap.map object;
  heap : NatPairMap.map object;
  limit : nat
}.

Definition initState :=
  Build_state
  (VarMap.init (0, false))
  (NatPairMap.init (0, false))
  0.

(** * Dynamic semantics *)

Definition exec (i : activity) (s : state) : state :=
  match i with
    | Allocate dst =>
      Build_state
      (VarMap.upd (vars s) dst (S (limit s), false))
      (heap s)
      (S (limit s))
    | Source dst =>
      Build_state
      (VarMap.upd (vars s) dst (S (limit s), true))
      (heap s)
      (S (limit s))
    | Copy dst src =>
      Build_state
      (VarMap.upd (vars s) dst (VarMap.sel (vars s) src))
      (heap s)
      (limit s)
    | Sanitize dst src =>
      Build_state
      (VarMap.upd (vars s) dst (cell (VarMap.sel (vars s) src), false))
      (heap s)
      (limit s)
    | Read dst src fl =>
      match NatPairMap.sel (heap s) ((cell (VarMap.sel (vars s) src)), fl) with
	| (O, _) => s
	| addr =>
	  Build_state
	  (VarMap.upd (vars s) dst addr)
	  (heap s)
	  (limit s)
      end
    | Write dst fl src =>
      match cell (VarMap.sel (vars s) dst) with
	| O => s
	| addr =>
	  Build_state
	  (vars s)
	  (NatPairMap.upd (heap s) (addr, fl) (VarMap.sel (vars s) src))
	  (limit s)
      end
  end.

(** * Must-not-be-tainted relation *)

Section mustNotBeTainted.
  Variable process : compound_activity activity.
  Variable v : var.

  Definition mustNotBeTainted :=
    forall c s, reachable exec process process initState c s
      -> cell (VarMap.sel (vars s) v) <> 0
        -> taint (VarMap.sel (vars s) v) = false.

  Inductive mustNotBeTainted_answer : Set :=
    | MaybeTainted : mustNotBeTainted_answer
    | NotTainted : mustNotBeTainted -> mustNotBeTainted_answer.
End mustNotBeTainted.

Definition mustNotBeTainted_procedure := forall process v,
  mustNotBeTainted_answer process v.
